<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FilesController extends Controller
{
    public function index()
    {
        return response()->json(Storage::files('files'), 200);
    }

    public function show($file)
    {
        return response()->download(storage_path('app/files/x' . $file));
    }

    public function store(Request $request)
    {
        $path = $request->file('file')->store('files');

        return response()->json($path, 200);
    }
}
