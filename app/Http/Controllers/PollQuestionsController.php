<?php

namespace App\Http\Controllers;

use App\Poll;
use Illuminate\Http\Request;

class PollQuestionsController extends Controller
{
    public function index(Poll $poll)
    {
        return response()->json($poll->questions, 200);
    }
}
