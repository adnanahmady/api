<?php

namespace App\Http\Controllers;

use App\Http\Requests\PollsRequest;
use App\Http\Resources\PollResource;
use App\Poll;
use Illuminate\Http\Request;

class PollsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Poll::get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PollsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $poll = Poll::create($request->all());

        return response()->json($poll, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  Poll  $poll
     * @return \Illuminate\Http\Response
     */
    public function show(Poll $poll)
    {
        return response()->json(new PollResource($poll));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PollsRequest  $request
     * @param  Poll $poll
     * @return \Illuminate\Http\Response
     */
    public function update(PollsRequest $request, Poll $poll)
    {
        $poll->update($request->all());

        return response()->json($poll, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Poll $poll
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Poll $poll)
    {
        $poll->delete();

        return response()->json(null, 204);
    }
}
