<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class QuestionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->resource->title,
            'question' => $this->resource->question,
            'poll_id' => $this->resource->poll_id,
            'questioned_at' => $this->resource->created_at->diffForHumans()
        ];
    }
}
