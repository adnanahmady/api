<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function() {
    Route::apiResource('polls', 'PollsController');
    Route::apiResource('questions', 'QuestionsController');
    Route::get('polls/{poll}/questions', 'PollQuestionsController@index');
    Route::get('files', 'FilesController@index');
    Route::get('files/{file}', 'FilesController@show');
    Route::post('files', 'FilesController@store');
});
